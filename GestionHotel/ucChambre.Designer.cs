﻿namespace GestionHotel
{
    partial class ucChambre
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblNum = new System.Windows.Forms.Label();
            this.Menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.reserverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.libererToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNum
            // 
            this.lblNum.AutoSize = true;
            this.lblNum.BackColor = System.Drawing.Color.Gray;
            this.lblNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNum.ForeColor = System.Drawing.Color.White;
            this.lblNum.Location = new System.Drawing.Point(4, 4);
            this.lblNum.Name = "lblNum";
            this.lblNum.Size = new System.Drawing.Size(24, 25);
            this.lblNum.TabIndex = 0;
            this.lblNum.Text = "0";
            // 
            // Menu
            // 
            this.Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reserverToolStripMenuItem,
            this.libererToolStripMenuItem,
            this.etatToolStripMenuItem});
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(211, 104);
            // 
            // reserverToolStripMenuItem
            // 
            this.reserverToolStripMenuItem.Name = "reserverToolStripMenuItem";
            this.reserverToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.reserverToolStripMenuItem.Text = "Reserver";
            this.reserverToolStripMenuItem.Click += new System.EventHandler(this.reserverToolStripMenuItem_Click);
            // 
            // libererToolStripMenuItem
            // 
            this.libererToolStripMenuItem.Name = "libererToolStripMenuItem";
            this.libererToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.libererToolStripMenuItem.Text = "Liberer";
            this.libererToolStripMenuItem.Click += new System.EventHandler(this.libererToolStripMenuItem_Click);
            // 
            // etatToolStripMenuItem
            // 
            this.etatToolStripMenuItem.Name = "etatToolStripMenuItem";
            this.etatToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.etatToolStripMenuItem.Text = "Etat";
            this.etatToolStripMenuItem.Click += new System.EventHandler(this.etatToolStripMenuItem_Click);
            // 
            // ucChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ContextMenuStrip = this.Menu;
            this.Controls.Add(this.lblNum);
            this.Name = "ucChambre";
            this.Size = new System.Drawing.Size(190, 77);
            this.Load += new System.EventHandler(this.ucChambre_Load);
            this.Menu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNum;
        private System.Windows.Forms.ContextMenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem reserverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem libererToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatToolStripMenuItem;
    }
}
