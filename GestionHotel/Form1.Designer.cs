﻿namespace GestionHotel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucChambre1 = new GestionHotel.ucChambre();
            this.ucChambre2 = new GestionHotel.ucChambre();
            this.ucChambre3 = new GestionHotel.ucChambre();
            this.ucChambre4 = new GestionHotel.ucChambre();
            this.ucChambre5 = new GestionHotel.ucChambre();
            this.ucChambre6 = new GestionHotel.ucChambre();
            this.ucChambre7 = new GestionHotel.ucChambre();
            this.ucChambre8 = new GestionHotel.ucChambre();
            this.ucChambre9 = new GestionHotel.ucChambre();
            this.SuspendLayout();
            // 
            // ucChambre1
            // 
            this.ucChambre1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre1.Disponible = false;
            this.ucChambre1.Location = new System.Drawing.Point(12, 12);
            this.ucChambre1.Name = "ucChambre1";
            this.ucChambre1.Numero = 7;
            this.ucChambre1.Size = new System.Drawing.Size(190, 77);
            this.ucChambre1.TabIndex = 0;
            // 
            // ucChambre2
            // 
            this.ucChambre2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre2.Disponible = true;
            this.ucChambre2.Location = new System.Drawing.Point(208, 12);
            this.ucChambre2.Name = "ucChambre2";
            this.ucChambre2.Numero = 8;
            this.ucChambre2.Size = new System.Drawing.Size(190, 77);
            this.ucChambre2.TabIndex = 0;
            // 
            // ucChambre3
            // 
            this.ucChambre3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre3.Disponible = false;
            this.ucChambre3.Location = new System.Drawing.Point(404, 12);
            this.ucChambre3.Name = "ucChambre3";
            this.ucChambre3.Numero = 9;
            this.ucChambre3.Size = new System.Drawing.Size(190, 77);
            this.ucChambre3.TabIndex = 0;
            // 
            // ucChambre4
            // 
            this.ucChambre4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre4.Disponible = true;
            this.ucChambre4.Location = new System.Drawing.Point(12, 95);
            this.ucChambre4.Name = "ucChambre4";
            this.ucChambre4.Numero = 4;
            this.ucChambre4.Size = new System.Drawing.Size(190, 77);
            this.ucChambre4.TabIndex = 0;
            // 
            // ucChambre5
            // 
            this.ucChambre5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre5.Disponible = false;
            this.ucChambre5.Location = new System.Drawing.Point(208, 95);
            this.ucChambre5.Name = "ucChambre5";
            this.ucChambre5.Numero = 5;
            this.ucChambre5.Size = new System.Drawing.Size(190, 77);
            this.ucChambre5.TabIndex = 0;
            // 
            // ucChambre6
            // 
            this.ucChambre6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre6.Disponible = true;
            this.ucChambre6.Location = new System.Drawing.Point(404, 95);
            this.ucChambre6.Name = "ucChambre6";
            this.ucChambre6.Numero = 6;
            this.ucChambre6.Size = new System.Drawing.Size(190, 77);
            this.ucChambre6.TabIndex = 0;
            // 
            // ucChambre7
            // 
            this.ucChambre7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre7.Disponible = false;
            this.ucChambre7.Location = new System.Drawing.Point(12, 178);
            this.ucChambre7.Name = "ucChambre7";
            this.ucChambre7.Numero = 1;
            this.ucChambre7.Size = new System.Drawing.Size(190, 77);
            this.ucChambre7.TabIndex = 0;
            // 
            // ucChambre8
            // 
            this.ucChambre8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre8.Disponible = false;
            this.ucChambre8.Location = new System.Drawing.Point(208, 178);
            this.ucChambre8.Name = "ucChambre8";
            this.ucChambre8.Numero = 2;
            this.ucChambre8.Size = new System.Drawing.Size(190, 77);
            this.ucChambre8.TabIndex = 0;
            // 
            // ucChambre9
            // 
            this.ucChambre9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ucChambre9.Disponible = false;
            this.ucChambre9.Location = new System.Drawing.Point(404, 178);
            this.ucChambre9.Name = "ucChambre9";
            this.ucChambre9.Numero = 3;
            this.ucChambre9.Size = new System.Drawing.Size(190, 77);
            this.ucChambre9.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(617, 272);
            this.Controls.Add(this.ucChambre9);
            this.Controls.Add(this.ucChambre6);
            this.Controls.Add(this.ucChambre3);
            this.Controls.Add(this.ucChambre8);
            this.Controls.Add(this.ucChambre5);
            this.Controls.Add(this.ucChambre2);
            this.Controls.Add(this.ucChambre7);
            this.Controls.Add(this.ucChambre4);
            this.Controls.Add(this.ucChambre1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private ucChambre ucChambre1;
        private ucChambre ucChambre2;
        private ucChambre ucChambre3;
        private ucChambre ucChambre4;
        private ucChambre ucChambre5;
        private ucChambre ucChambre6;
        private ucChambre ucChambre7;
        private ucChambre ucChambre8;
        private ucChambre ucChambre9;
    }
}

