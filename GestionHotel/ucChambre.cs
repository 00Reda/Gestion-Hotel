﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionHotel
{
    public partial class ucChambre : UserControl
    {
        public int Numero { get; set; }
        public bool Disponible { get; set; }

        public ucChambre()
        {
            InitializeComponent();
        }

        private void ucChambre_Load(object sender, EventArgs e)
        {
            lblNum.Text = this.Numero.ToString();
            Afficher();
        }

        private void Afficher()
        {
            this.BackColor = this.Disponible ? Color.Yellow : Color.Red;
            reserverToolStripMenuItem.Visible = Disponible;
            libererToolStripMenuItem.Visible = !Disponible;
        }

        private void reserverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Disponible = false;
            Afficher();

        }

        private void libererToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Disponible = true;
            Afficher();
        }

        private void etatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string etat = Disponible ? "disponible" : "occupee";
            MessageBox.Show($"est {etat}",$"La chambre {Numero}",MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
